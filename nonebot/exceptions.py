from aiocqhttp import Error as CQHttpError

__all__ = [
    'CQHttpError',
]

__pdoc__ = {
    'CQHttpError': """
        等价于 `aiocqhttp.Error`。
        """
}
