---
contentSidebar: true
sidebarDepth: 0
---

# `nonebot.log` 模块

## _var_ `logger`

- **类型:** logging.Logger

- **说明:** NoneBot 全局的 logger。

- **用法**

```python
logger.debug('Some log message here')
```